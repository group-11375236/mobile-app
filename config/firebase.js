import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getStorage } from 'firebase/storage';
import { getFirestore } from 'firebase/firestore';
import { getReactNativePersistence } from 'firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';



// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA6dPfUvmlfuuxyF6GCJ5X055qs_R0Dkl4",
  authDomain: "communicare-auth-c4a15.firebaseapp.com",
  databaseURL: "https://communicare-auth-c4a15-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "communicare-auth-c4a15",
  storageBucket: "communicare-auth-c4a15.appspot.com",
  messagingSenderId: "701515357238",
  appId: "1:701515357238:web:dbb8fe6d7e85fa47afcc97"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const storage = getStorage(app);
const firestore = getFirestore(app, { persistence: getReactNativePersistence(AsyncStorage) });

export { auth, storage, firestore, app };