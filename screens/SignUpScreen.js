import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native'
import React, { useState } from 'react'
import { themeColors } from '../theme'
import { SafeAreaView } from 'react-native-safe-area-context'
import {ArrowLeftIcon} from 'react-native-heroicons/solid';
import { useNavigation } from '@react-navigation/native';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../config/firebase';


// subscribe for more videos like this :)
export default function SignUpScreen() {
    const navigation = useNavigation();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [fullName, setName] = useState('');
    const [passwordsMatch, setPasswordsMatch] = useState(true);
    const [isSecureEntry, setIsSecureEntry] = useState(true);
    const [confirmPassword, setConfirmPassword] = useState(''); 

    const handlePasswordChange = (value) => {
        setPassword(value);
        setPasswordsMatch(value === confirmPassword);
    };
    
    const handleConfirmPasswordChange = (value) => {
        setConfirmPassword(value);
        setPasswordsMatch(value === password);
    };

    const handleSubmit = async () => {
        if (email && password && passwordsMatch && fullName) {
            try {
                await createUserWithEmailAndPassword(auth, email, password, fullName);
            } catch (err) {
                console.log('got error: ', err.message);    
            }
        } else {
            if (!fullName) {
                alert('Please enter your Full Name');
            } else if (!email) {
                alert('Please enter your Email Address');
            } else if (!password) {
                alert('Please enter your Password');
            } else if (!confirmPassword) {
                alert('Please confirm your Password');
            } else if (!passwordsMatch) {
                alert('Password and Confirm Password must match');
            }
        }
    };

  return (
    <View className="flex-1 bg-white" style={{backgroundColor: themeColors.secondary}}>
      <SafeAreaView className="flex">
        <View className="flex-row justify-start">
        </View>
        <View className="flex-row justify-center">
            <Image source={require('../assets/images/Signup.png')} 
                style={{width: 305, height: 150}} />
        </View>
      </SafeAreaView>
      <View className="flex-1 bg-lime-100 px-8 pt-8"
        style={{borderTopLeftRadius: 50, borderTopRightRadius: 50}}
      >
        <View className="form space-y-2">
            <Text className="text-gray-700 ml-1">Full Name</Text>
            <TextInput
                className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3"
                value={fullName}
                onChangeText={value=> setName(value)}
                placeholder='Enter Name'
            />
            <Text className="text-gray-700 ml-1">Email Address</Text>
            <TextInput
                className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3"
                value={email}
                onChangeText={value=> setEmail(value)}
                placeholder='Enter Email'
            />
            <Text className="text-gray-700 ml-1">Password</Text>
            <TextInput
                className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-7"
                secureTextEntry={isSecureEntry}
                value={password}
                onChangeText={handlePasswordChange}
                placeholder='Enter Password'
            />
            <TouchableOpacity
                        onPress={() => {
                            setIsSecureEntry((prev) => !prev);
                        }}
                        style={{ position: 'absolute', right: 20, top: 255 }} 
                    >
                        <Text>{isSecureEntry ? "Show" : "Hide"}</Text>
                    </TouchableOpacity>
                    <Text className="text-gray-700 ml-1">Confirm Password</Text>
                    <TextInput
                        className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-7"
                        secureTextEntry={isSecureEntry}
                        value={confirmPassword}
                        onChangeText={handleConfirmPasswordChange}
                        placeholder="Confirm Password"
                    />
                    <TouchableOpacity
                        onPress={() => {
                            setIsSecureEntry((prev) => !prev);
                        }}
                        style={{ position: 'absolute', right: 20, top: 375}} 
                    >
                        <Text>{isSecureEntry ? "Show" : "Hide"}</Text>
                    </TouchableOpacity>
                    {!passwordsMatch && (
                        <Text className="text-red-500 ml-4">Passwords do not match</Text>
                    )}
            <TouchableOpacity
                className="py-3 bg-amber-200 rounded-xl"
                onPress={handleSubmit}
            >
                <Text className="font-xl font-bold text-center text-gray-700">
                    Sign Up
                </Text>
            </TouchableOpacity>
        </View>
        <View className="flex-row justify-center mt-7">
            <Text className="text-gray-500 font-semibold">Already have an account?</Text>
            <TouchableOpacity onPress={()=> navigation.navigate('Login')}>
                <Text className="font-semibold text-yellow-500"> Login</Text>
            </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}
