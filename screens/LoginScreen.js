import * as React from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { themeColors } from '../theme/index';
import { useNavigation } from '@react-navigation/native';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../config/firebase';
import * as WebBrowser from 'expo-web-browser';
import { makeRedirectUri, useAuthRequest } from 'expo-auth-session';
import { GoogleAuthProvider, signInWithCredential } from 'firebase/auth';

const GoogleIcon = require('../assets/icons/google.png');
WebBrowser.maybeCompleteAuthSession();

const YOUR_CLIENT_ID = '701515357238-rjem0ep32cqs8se9uqus5vtpc8skgfck.apps.googleusercontent.com';  // Replace with your actual client ID
const YOUR_REDIRECT_URI = 'https://communicare-auth-c4a15.firebaseapp.com/__/auth/handler';  // Replace with your actual redirect URI

export default function LoginScreen() {
  const navigation = useNavigation();
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [isSecureEntry, setIsSecureEntry] = React.useState(true);
  const [passwordsMatch, setPasswordsMatch] = React.useState(true);

  const handleSubmit = async () => {
    if (email && password && passwordsMatch) {
      try {
        await signInWithEmailAndPassword(auth, email, password);
      } catch (err) {
        console.log('got error: ', err.message);
        alert('Login failed. Your email and password do not match.');
      }
    } else {
      if (!email) {
        alert('Please enter your Email Address');
      }
      if (!password) {
        alert('Please enter your Password');
      }
      if (!passwordsMatch) {
        alert('Password does not match');
      }
    }
  };

  const discovery = {
    authorizationEndpoint: 'https://accounts.google.com/o/oauth2/v2/auth',
    tokenEndpoint: 'https://oauth2.googleapis.com/token',
  };

  const [request, response, promptAsync] = useAuthRequest(
    {
      clientId: YOUR_CLIENT_ID,
      scopes: ['openid', 'profile', 'email'],
      redirectUri: YOUR_REDIRECT_URI,
    },
    discovery
  );

  React.useEffect(() => {
    if (response?.type === 'success') {
      const { authentication } = response;
      console.log('Google login successful. AccessToken:', authentication.accessToken);
    }
  }, [response]);

  const handleGoogleLogin = async () => {
    try {
      await promptAsync();
    } catch (error) {
      console.log('Google login failed. Error:', error);
    }
  };

  return (
    <View className="flex-1 bg-white" style={{ backgroundColor: themeColors.secondary }}>
      <SafeAreaView className="flex ">
        <View className="flex-row justify-start" />
        <View className="flex-row justify-center">
          <Image source={require('../assets/images/login.png')} style={{ width: 390, height: 220 }} />
        </View>
      </SafeAreaView>
      <View style={{ borderTopLeftRadius: 50, borderTopRightRadius: 50 }} className="flex-1 bg-lime-100 px-8 pt-8">
        <View className="form space-y-1">
          <Text className="text-gray-700 ml-1">Email Address</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl mb-3"
            placeholder="Enter your Email"
            value={email}
            onChangeText={value => setEmail(value)}
          />
          <Text className="text-gray-700 ml-1">Password</Text>
          <TextInput
            className="p-4 bg-gray-100 text-gray-700 rounded-2xl"
            secureTextEntry={isSecureEntry}
            placeholder="Enter your Password"
            value={password}
            onChangeText={value => setPassword(value)}
          />
          <TouchableOpacity
            onPress={() => {
              setIsSecureEntry(prev => !prev);
            }}
            style={{ position: 'absolute', right: 20, top: 140 }}
          >
            <Text>{isSecureEntry ? "Show" : "Hide"}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')} className="flex items-end">
            <Text className="text-gray-700 mb-5">Forgot Password?</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleSubmit} className="py-3 bg-amber-200 rounded-xl">
            <Text className="text-xl font-bold text-center text-gray-700">Login</Text>
          </TouchableOpacity>
        </View>
        <View className="flex-row justify-center mt-7">
          <Text className="text-gray-500 font-semibold">Don't have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
            <Text className="font-semibold text-yellow-500"> Sign Up</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
          <View style={{ borderBottomWidth: 1, flex: 1, borderColor: 'gray' }} />
          <Text style={{ marginHorizontal: 10, color: 'gray' }}>OR</Text>
          <View style={{ borderBottomWidth: 1, flex: 1, borderColor: 'gray' }} />
        </View>
        <TouchableOpacity onPress={handleGoogleLogin} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
          <Image source={GoogleIcon} style={{ width: 20, height: 20, marginRight: 10 }} />
          <Text style={{ color: 'gray' }}>Login with Google</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}