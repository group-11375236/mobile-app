import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, StyleSheet, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getDatabase, ref, push, onValue } from 'firebase/database';
import { getAuth } from 'firebase/auth';

const database = getDatabase();

const ChatScreen = () => {
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');

  useEffect(() => {
    const chatRef = ref(database, 'messages');
    onValue(chatRef, (snapshot) => {
      if (snapshot.exists()) {
        const data = snapshot.val();
        setMessages(Object.values(data));
      }
    });
  }, []);

  const renderMessages = () => {
    return messages.map((item, index) => (
      <View
        key={index}
        style={item.isAdmin ? styles.adminMessageContainer : styles.userMessageContainer}
      >
        <Text style={item.isAdmin ? styles.adminMessageText : styles.userMessageText}>
          {item.text}
        </Text>
      </View>
    ));
  };

  const sendMessage = () => {
    if (newMessage.trim() !== '') {
      const isAdmin = false;
      const userId = getAuth().currentUser.uid;
      const newMessageRef = push(ref(database, 'messages'));
      newMessageRef.set({
        text: newMessage,
        timestamp: new Date().toISOString(),
        isAdmin,
        userId,
      });
      setNewMessage('');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={styles.messagesContainer}>
        {renderMessages()}
      </ScrollView>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          onChangeText={(text) => setNewMessage(text)}
          value={newMessage}
          placeholder="Type a message..."
        />
        <Button title="Send" onPress={sendMessage} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  messagesContainer: {
    flexGrow: 1,
    paddingBottom: 10,
    paddingHorizontal: 16,
  },
  adminMessageContainer: {
    alignSelf: 'flex-start',
    marginBottom: 10,
    backgroundColor: '#f2f2f2',
    borderRadius: 10,
    padding: 10,
    maxWidth: '70%',
  },
  userMessageContainer: {
    alignSelf: 'flex-end',
    marginBottom: 10,
    backgroundColor: '#5AC8FA',
    borderRadius: 10,
    padding: 10,
    maxWidth: '70%',
  },
  adminMessageText: {
    fontSize: 16,
  },
  userMessageText: {
    fontSize: 16,
    color: 'white',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'gray',
    paddingHorizontal: 10,
    paddingBottom: 60,  // Adjust as needed
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    flex: 1,
    marginRight: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
  },
});

export default ChatScreen;