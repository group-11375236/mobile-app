import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const developerData = [
  { name: 'Patrick Josh Antonio', role: 'Front End/Back End', image: require('../assets/images/patrick.jpg') },
  { name: 'Ricarte Genese Naraja', role: 'Front End', image: require('../assets/images/ricarte.webp') },
  { name: 'Miguel Umali Cerezo', role: 'Gatherer Resources', image: require('../assets/images/miguel.webp') },
  { name: 'Omar Vincent Manuel Buena', role: 'Gatherer Resources', image: require('../assets/images/omar.webp') },
  { name: 'Vincent Macabalos Angeles', role: 'Gatherer Resources', image: require('../assets/images/vincent.webp') },
];

const AboutUs = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>CommuniCare</Text>
      <Text style={styles.introText}>
       Welcome to CommuniCare, an innovative application dedicated to revolutionizing community healthcare. Our team of skilled developers has come together to create a secure and intuitive platform, providing advanced security solutions for an enhanced healthcare experience.
      </Text>

      <Text style={styles.developerTitle}>Meet the Developers:</Text>
      {developerData.map((developer, index) => (
        <View key={index} style={styles.developerContainer}>
          <Image source={developer.image} style={styles.avatar} />
          <View style={styles.developerInfo}>
            <Text style={styles.name}>{developer.name}</Text>
            <Text style={styles.role}>{developer.role}</Text>
          </View>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#101820FF'
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    color: '#AEE8F5'
  },
  introText: {
    marginBottom: 20,
    color: 'white'
  },
  developerTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    color: '#AEE8F5'
  },
  developerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginRight: 10,
  },
  developerInfo: {
    flexDirection: 'column',
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#AEE8F5'
  },
  role: {
    fontSize: 15,
    color: 'white'
  },
});

export default AboutUs;
